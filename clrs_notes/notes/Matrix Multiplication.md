### Standard Algorithm:
![[Pasted image 20231115145808.png]]

### [[Divide and Conquer]] Method:
- divide into 4 square matrices
- solve recursively
![[Pasted image 20231115150057.png]]
[[Recurrence equation]] Analysis:
Base case for $n=1: \Theta(n)=1$
Recursive case for everything $n >1$.
8 recursive steps, each multiplying a $\frac{n}{2}\times \frac{n}{2}$ matrix, so $8T(n/2)$
no combine step
so: $$T(n)=8T(n/2)+\Theta(1)$$


### Strassen’s Algorithm
- work for each divide and combine step increases, but it pays off
- only 7 instead of 8 recursive calls
- make 11 matrices: additions/subtractions of 2 submatrices, the other ones are all zeroed
- perform 7 multiplications
- add and subtract sub matrices in a particular way

