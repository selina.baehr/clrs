The three most common types of asymptotic notation are $\Theta$-Notation, $\Omega$-Notation and $O$-Notation.

## $O$-Notation
$O$-Notation characterizes an **upper bound** on the asymptotic behaviour of a function. In other words, it says that a function grows no faster than a certain rate, based on the highest-order term. Consider, for example, the function  $7n^3+100n^2-20n+6$. Its highest-order term is $n^3$ , and so we say that this function’s rate of growth is $O(n^3)$ .

## $\Omega$-Notation
$\Omega$-notation characterizes a **lower bound** on the asymptotic behavior of a function. In other words, it says that a function grows at least as fast as a certain rate, based - as in $O$-notation - on the highest-order term. Because the highest-order term in the function $7n^3+100n^2-20n+6$ grows at least as fast as $n^3$ , this function is $\Omega(n^3)$. This function is also $\Omega(n^2)$ and $\Omega(n)$.

## $\Theta$-Notation
$\Theta$-notation characterizes a **tight bound** on the asymptotic behavior of a function. It says that a function grows precisely at a certain rate, based - once again - on the highest-order term. Put another way, $\Theta$-notation characterizes the rate of growth of the function to within a constant factor from above and to within a constant factor from below. These two constant factors need not be equal. If a function is both $\Omega(f(n))$ and $O(f(n))$ , then its also $\Theta(f(n))$.

## $o$-Notation
$o$-Notation denotes an upper bound that is **not asymptotically tight**.
$f(n) = o(g(n))$ implies:
$$\lim_{n \to \infty} \frac{f(x)}{g(x)} = 0$$
## $\omega$-Notation
$\omega$-Notation is for $\Omega$-Notation what $o$-Notation is for $O$-Notation. 
$f(n) = \omega(g(n))$ implies:
$$\lim_{n \to \infty} \frac{f(x)}{g(x)} = \infty$$

Following properties hold for asymptotic notation:
- Transitivity
- Reflexivity
- Symmetry
- Transpose Symmetry
**Trichotomy does NOT hold for asymptotic notation!**

We can draw an analogy between comparing asymptotic notation and comparing real numbers:
![[Pasted image 20231027214559.png | 400]]

### Stirling's Approximation
... is quite cool!
$$n! = \sqrt{2\pi n}\Bigl(\frac{n}{e}\Bigl)^n \Bigl(1+\Theta \Bigl(\frac{1}{n}\Bigl)\Bigl)$$


