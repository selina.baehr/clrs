#sorting_algos
Insertion Sort's correctness can be proven using [[Loop invariants]]
### Pseudocode
![[Pasted image 20231027191311.png ]]

### Running Time Analysis
Best case: $\Theta (n)$
Worst case: $\Theta (n^2)$

_exercise 2.4_
The running time of insertion sort is a constant times the number of inversions. Let $I(i)$ denote the number of $j < i$ such that $A[j]>A[i]$. Then $∑_{i=1}^n I(i)$ equals the number of inversions in $A$. The while-loop will execute for every inversion.
