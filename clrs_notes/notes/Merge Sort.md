#sorting_algos
Merge sort is a good example for a sorting algorithm that uses the [[Divide and Conquer]] method. 
### Merge-Operation Pseudocode
![[Pasted image 20231027191732.png]]
### Pseudocode
![[Pasted image 20231027191815.png]]

### Running Time Analysis

**Divide:** The divide step just computes the middle of the subarray, which takes constant time. Thus, $D(n) = \Theta (1)$.
**Conquer:** Recursively solving two subproblems, each of size $n/2$ , contributes $2T(n/2)$ to the running time (ignoring the floors and ceilings).
**Combine:** Since the MERGE procedure on an $n$-element subarray takes $\Theta (n)$ time, we have  $C(n) = \Theta (n)$.

**Best case:** $\Theta (n \lg n)$
**Worst case:** $\Theta (n \lg n)$
The intuition for that is to imagine the algorithm as a **recursion tree** - which has $\lg n + 1$ levels.
![[Pasted image 20231027192634.png | 500]] 
