_page 20_
Loop invariants help us understand why an algorithm is correct. When you’re using a loop invariant, you need to show three things:

**Initialization:** It is true prior to the first iteration of the loop.  

**Maintenance:** If it is true before an iteration of the loop, it remains true before

**Termination:** The loop terminates, and when it terminates, the invariant - usually along with the reason that the loop terminated - gives us a useful property that helps show that the algorithm is correct.