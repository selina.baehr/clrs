… is **an equation that defines a sequence based on a rule that gives the next term as a function of the previous term(s)**.

## Solving Recurrences
### Substitution Method
1. Guess the form of the solution using symbolic constants.  
2. Use mathematical induction to show that the solution works, and find the constants
### Recursive Method
![[Pasted image 20231115161852.png | 500]]
![[Pasted image 20231115161948.png | 500]]
### [[Master method]]
