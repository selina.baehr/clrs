for solving recursive algorithms of the form:
$$T(n)=aT(n/b)+f(n)$$
with $a>0, b>1$. $f(n)$ is the **driving function.**

→ Master recurrence describes d&c algos that divide a problem of size $n$ into $a$ subproblems, each of size $n/b<n$ and solves them each in $T(n/b)$ time.
$f(n)$ expresses the cost of the division as well as the combining.
For example: [[Matrix Multiplication#Strassen’s Algorithm]]:
$$a=7,\ b =2, \ f(n)=\Theta(n^2)$$
### Definition
If you have a recurrence of the form seen above, then the asymptotic behaviour can be characterised as follows:
1. If there exists a constant $\epsilon >0$ such that $f(n)=O(n^{\log_{b}a-\epsilon})$, then $T(n)=\Theta(n^{\log_{b}a})$.
2. If there exists a constant $k \geq 0$ such that $f(n)=\Theta(n^{\log_{b}a}\lg^kn)$, then $T(n)=\Theta(n^{\log_{b}a}\lg^{k+1}n)$
3. If there exists a constant $\epsilon >0$ such that $f(n)=\Omega(n^{\log_{b}a-\epsilon})$, and if $f(n)$ additionally satisfies the **regularity condition** $af(n/b)\leq cf(n)$  for some constant $c <1$ and sufficiently large $n$, then $T(n)=\Theta(f(n))$.
#### watershed-function
$n^{\log_{b}a}$ is also called the **watershed-function**. 
In each of the cases we compare the driving function to the watershed-function. If the watershed-function grows faster, then case 1 applies. Case 2 applies if they grow at nearly the same asymptotic rate, case 3 applies if the driving function grows faster.
**case 1**
in case 1, not only does the w-function grow asymptotically faster, it has to grow **polynomially faster**, that means, it must be larger by at least a factor of $\Theta(n^\epsilon)$ for some $\epsilon$.
In this case, if we look at the recursion tree for the recurrence, the cost per level grows **at least geometrically** from root to leaves, and **the total cost of leaves dominates the total cost of the internal nodes.**
**case 2**
the driving function grows faster than the w-function by a factor of $\Theta(\lg^k)$

### Proof of continuous Master Theorem
![[Pasted image 20240216134122.png]]
Number of Leaves = $a^{\log_{b}n+1}=\Theta(n^{\log_{b}a})$
