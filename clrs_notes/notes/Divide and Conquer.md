
In the divide-and-conquer method, if the problem is small enough - the base case - you just solve it directly without recursing. Otherwise - the recursive case - you perform three characteristic steps:

**Divide** the problem into one or more subproblems that are smaller instances of the same problem.
**Conquer** the subproblems by solving them recursively.  
**Combine** the subproblem solutions to form a solution to the original problem.



### Analysis of Divide-and-Conquer Algorithms

When an algorithm contains a recursive call, you can often describe its running time by a [[Recurrence equation]] , which describes the overall running time on a problem of size in terms of the running time of the same algorithm on smaller inputs. 

A recurrence $T$ is algorithmic if, for every sufficiently large threshold constant  $n_0 >0$, the following two properties hold:

1. For all $n < n_{0}$  , we have $T(n)=\Theta(1)$.
2. For all $n \geq n_{0}$ every path of recursion terminates in a defined base case within a finite number of recursive calls.
A recurrence describing a correct D&C algorithms’ worst case is algorithmic (page 77 for proof).
A Recurrence can also be represented by an equation; in that case it represents the upper/lower threshold.



