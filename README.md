### Reading "Introduction to Algorithms" (CLRS)
#### Currently Reading: Chapter 4 Divide-and-Conquer
!["big red" as seen on the cover on the book.](https://i.ibb.co/MBcP5Hc/big-red.png) \
_"Big Red" by Alexander Calder; as seen on the cover of the book._
#
this repository is meant to document and track my read of CLRS. Throughout my read I will take notes in an **Obsidian Vault** as well as (hopefully) implement various algorithms that I encounter. 

Solutions to exercises: https://sites.math.rutgers.edu/~ajl213/CLRS/CLRS.html
https://github.com/CyberZHG/CLRS/blob/master/Chapter_03_Growth_of_Functions/problems.md



